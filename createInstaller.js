const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller

const path = require('path')

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error)
    process.exit(1)
  });

function getInstallerConfig () {
  console.log('creating windows installer')
  const rootPath = path.join('./')
  const outPath = path.join(rootPath, 'release-builds')

  return Promise.resolve({
    appDirectory: path.join(outPath, 'Pow! Comics Reader-win32-x64'),
    authors: 'fox',
    noMsi: true,
    outputDirectory: path.join(outPath, 'installer'),
    exe: 'Pow! Comics Reader.exe',
    setupExe: 'PowComicsReaderSetup.exe',
    setupIcon: path.join(rootPath, 'img', 'ic_launcher.ico')
  });
}
