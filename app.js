const setupEvents = require('./setupEvents')

if (setupEvents.handleSquirrelEvent()) {
	// squirrel event handled and app will exit in 1000ms, so don't do anything else
	return;
}

// Modules to control application life and create native browser window
const {app, BrowserWindow, Menu} = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow() {
	// Create the browser window.
	mainWindow = new BrowserWindow({
		icon: 'img/ic_launcher.png',
		width: 1280,
		height: 1024,
		webPreferences: {
			nodeIntegration: true,
			contextIsolation: false,
			enableRemoteModule: true
		}
	});

	//mainWindow.maximize();

	const menu = Menu.buildFromTemplate([
		{
			label: '&File',
			id: 'M-FILE',
			submenu: [
				// filled at runtime
			]
		},
		{
			label: '&Location',
			submenu: [
				{id: 'F-GO', label: '&Go to...', accelerator: 'Ctrl+G', click: () => { mainWindow.webContents.send("open-location"); }},
				{id: 'F-SYNC', label: '&Sync to last page read', click: () => { mainWindow.webContents.send("sync-to-last"); }},
				{type: 'separator'},
				{id: 'F-CLEAR', label: '&Clear last read', click: () => { mainWindow.webContents.send("clear-last-read"); }},
				{id: 'F-MARK', label: '&Mark as read', accelerator: 'Ctrl+M', click: () => { mainWindow.webContents.send("mark-as-read"); }},
			]
		},
		{
			label: '&Reading',
			submenu: [
				{id: 'F-FIT', label: 'Fit to &width', type: 'checkbox', accelerator: 'Alt+W', click: () => { mainWindow.webContents.send("fit-to-width"); }},
				{type: 'separator'},
				{id: 'F-SINGLE', label: '&Single column', type: 'checkbox', accelerator: 'Alt+S', click: () => { mainWindow.webContents.send("single-column"); }},
				{id: 'F-FLIP', label: '&Mirror (manga mode)', type: 'checkbox', accelerator: 'Alt+M', click: () => { mainWindow.webContents.send("flip-columns"); }},
				{type: 'separator'},
				{id: 'F-ZOOM-IN', label: 'Zoom &in', accelerator: 'Ctrl+=', click: () => { mainWindow.webContents.send("zoom-in"); }},
				{id: 'F-ZOOM-OUT', label: 'Zoom &out', accelerator: 'Ctrl+-', click: () => { mainWindow.webContents.send("zoom-out"); }},
				{id: 'F-ZOOM-RESET', label: '&Reset zoom', accelerator: 'Ctrl+0', click: () => { mainWindow.webContents.send("zoom-reset"); }},

			]
		},

		{
			label: '&Tools',
			submenu: [
				{label: '&Options', accelerator: 'Ctrl+Shift+O', click: () => { mainWindow.webContents.send("open-settings"); }},
				{label: 'Toggle &fullscreen', accelerator: 'F11', role: 'toggleFullScreen' },
				{type: 'separator'},
				{label: '&Developer tools', accelerator: 'F12', role: 'toggleDevTools' },
			]
		},
	]);

	Menu.setApplicationMenu(menu);

	// and load the reader.html of the app.
	mainWindow.loadFile('reader.html');

	// Open the DevTools.
	//mainWindow.webContents.openDevTools();

	// Emitted when the window is closed.
	mainWindow.on('closed', function () {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		mainWindow = null
	});
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
	// On macOS it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit()
	}
});

app.on('activate', function () {
	// On macOS it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow()
	}
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
