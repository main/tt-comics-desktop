pipeline {
    agent any

    environment {
        deploy_key = "srv.tt-rss.org"
        deploy_host = "tt-rss.fakecake.org"
    }

    stages {
        stage('build') {
            steps {
                sh """
                    docker run --rm --user `id -u` \
								-v ${env.WORKSPACE}:/app --workdir /app \
                        adactive/docker-electron-installer-windows \
                        sh -c 'unset WINEPREFIX && export HOME=/app && \
                        npm install && npm run package-lin && \
                        npm run package-win && \
                        npm run create-installer-win'
                """
            }
        }
        stage('compress') {
            steps {
                dir('release-builds') {
                    sh """
                    tar -czf 'Pow! Comics Reader-linux-x64.tgz' 'Pow! Comics Reader-linux-x64'
                    tar -czf 'Pow! Comics Reader-win32-x64.tgz' 'Pow! Comics Reader-win32-x64'
                    """
                }
            }
        }
        stage('archive') {
            steps {
                dir('release-builds') {
                    archiveArtifacts 'installer/*.exe, *.tgz'
                }
            }
        }
        stage('deploy') {
            steps {
                dir('release-builds') {
                    sshagent(credentials: ["${deploy_key}"]) {
                        script {
                            def files = findFiles(glob: '**/installer/*.exe, *.tgz')

                            for (String file : files) {
                                sh("scp -oStrictHostKeyChecking=no \"${file}\" ${deploy_host}:builds/tt-comics/")
                            }
                        }
                    }
                }
            }
        }
        stage('cleanup') {
            steps {
                sh """
                rm -rfv -- release-builds
                """
            }
        }
    }

    post {
        failure {
             mail body: "Project: ${env.JOB_NAME} <br>Build Number: ${env.BUILD_NUMBER}<br> build URL: ${env.BUILD_URL}",
                charset: 'UTF-8', from: 'jenkins@fakecake.org',
                mimeType: 'text/html',
                subject: "Build failed: ${env.JOB_NAME}",
                to: "fox@fakecake.org";
         }
    }
}
