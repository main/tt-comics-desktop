Pow! Comics Reader (desktop)
============================

Minimalistic CBZ reader, quasi-desktop version using Electron. Supports network sync, etc.

Building
========

```sh
npm install
npm run package-win # or package-lin
npm run create-installer-win
```

License
=======

GNU GPL version 3.
